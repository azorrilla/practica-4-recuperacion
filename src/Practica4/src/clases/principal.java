package clases;

import java.time.LocalDate;

public class Principal {

	public static void main(String[] args) {

		GestorContabilidad gestor = new GestorContabilidad();
		Cliente cliente1 = new Cliente ("David", "123456", LocalDate.now());
		Factura factura1 = new Factura("AA11", LocalDate.now(), "Videojuego", 2.5F, 3, cliente1);
		Cliente cliente2 = new Cliente ("Fernando", "11223344", LocalDate.now());
		Factura factura2 = new Factura("BBBB", LocalDate.now(), "Pelicula", 20, 2, cliente2);
		
		gestor.listaClientes.add(cliente1);
		gestor.listaClientes.add(cliente2);
		gestor.listaFacturas.add(factura1);
		gestor.listaFacturas.add(factura2);
		
		System.out.println(gestor.calcularFacturacionAnual(2018));

	}

}
